<?php
/**
 * $image - contains the image html rendered by Drupal
 * $caption - contains the image field caption string
 */
?>
<figure class="image_field">
  <?php print $image; ?>
  <figcaption class="image-field-caption">
    <?php print $caption; ?>
  </figcaption>
</figure>
